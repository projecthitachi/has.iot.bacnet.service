﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.IO.BACnet;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace has.iot.bacnet.service
{
    public partial class Service1 : ServiceBase
    {
        static BacnetClient client;
        private static MqttClient MqttClient = null;
        string PatchFile;
        // All the present Bacnet Device List
        static List<BacNode> DevicesList = new List<BacNode>();
        private static IList<BacnetValue> value_list;
        bool btn = true;
        int interv = 3000;
        public Service1()
        {
            InitializeComponent();
        }
        public void OnDebug()
        {
            OnStart(null);
        }
        protected override void OnStart(string[] args)
        {
            string poDeviceID = "";
            try
            {
                string MQTTHost = ConfigurationManager.AppSettings["MQTTHost"].ToString();
                PatchFile = ConfigurationManager.AppSettings["PatchFile"].ToString();
                string basepatch = AppDomain.CurrentDomain.BaseDirectory+"AppSetting.json";
                if (File.Exists(basepatch))
                {
                    string Json = File.ReadAllText(basepatch);
                    if (Json != "null")
                    {
                        object rootresult = JsonConvert.DeserializeObject(Json);
                        JObject rootobj = JObject.Parse(rootresult.ToString());
                        string poMQTT = rootobj["MQTTHOST"].ToString();
                        string poPatch = rootobj["PatchFile"].ToString();
                        MQTTHost = poMQTT;
                        PatchFile = poPatch;
                    }
                }
                
                // mqtt connection 
                MqttClient = new MqttClient(MQTTHost);
                MqttClient.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
                MqttClient.Subscribe(new string[] { "Bacnet/Raspberry/#" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                string clientId = Guid.NewGuid().ToString();
                MqttClient.Connect(clientId);


                void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
                {
                    var message = System.Text.Encoding.Default.GetString(e.Message);
                    if (IsValidJson(message))
                    {
                        string[] topic = e.Topic.Split('/');
                        string Category = topic[0];
                        string hostDevice = topic[1];
                        if (topic.Length > 1)
                        {
                            string cmd = topic[2];
                            switch (cmd)
                            {
                                case "Read":
                                    ReadToDevice(message);
                                    break;
                                case "Write":
                                    WriteToDevice(message);
                                    break;
                                case "Whois":
                                    whoisDevice(message);
                                    break;
                                case "SetConfig":
                                    setConfig(message);
                                    break;
                                case "GetConfig":
                                    getConfig(message);
                                    break;
                                case "SetInterval":
                                    object result = JsonConvert.DeserializeObject(message);
                                    JObject voobj = JObject.Parse(result.ToString());
                                    interv = Convert.ToInt32(voobj["Interval"].ToString());
                                    break;
                                default:
                                    
                                    break;
                            }
                        }
                    }
                }

                BacnetIpUdpProtocolTransport transport = new BacnetIpUdpProtocolTransport(0xBAC0);
                client = new BacnetClient(transport);
                client.OnIam += OnIAm;
                client.Start();
                client.WhoIs();

                Thread t = new Thread(new ThreadStart(ReadIntv));
                t.Start();
            }
            catch (Exception e)
            {
                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("deviceID", poDeviceID);
                voResData.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                MqttClient.Publish("Bacnet/Raspberry/" + poDeviceID + "/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);

                Process currentProcess = Process.GetCurrentProcess();
                int pid = currentProcess.Id;
                string applicationName = currentProcess.ProcessName;
                RestartApp(pid, applicationName);
                System.Environment.Exit(1);
            }
        }

        public void ReadIntv()
        {
            string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
            if (File.Exists(basepatch))
            {
                string Json = File.ReadAllText(basepatch);
                if (Json != "null")
                {
                    object rootresult = JsonConvert.DeserializeObject(Json);
                    JObject rootobj = JObject.Parse(rootresult.ToString());
                    interv = Convert.ToInt32(rootobj["Interval"].ToString());
                }
            }

            bool ret;
            while (true)
            {
                foreach (BacNode bn in DevicesList)
                {
                    // Read file json
                    string poDevice = bn.device_id.ToString();
                    string targetPathView = AppDomain.CurrentDomain.BaseDirectory + "Bacnet\\" + poDevice + ".json";
                    if (File.Exists(targetPathView))
                    {
                        string Json = File.ReadAllText(targetPathView);
                        if (Json != "null")
                        {
                            object rootresult = JsonConvert.DeserializeObject(Json);
                            JArray rootobj = JArray.Parse(rootresult.ToString());
                            int voDevice = Convert.ToInt32(bn.device_id);

                            BacnetValue Value;
                            Dictionary<string, string> ResData = new Dictionary<string, string>();
                            foreach (string validObj in rootobj)
                            {
                                string[] oi = validObj.Split(':');
                                BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), oi[0]);
                                uint instance = Convert.ToUInt32(oi[1]);
                                ret = ReadScalarValue(voDevice, new BacnetObjectId(ObjType, instance), BacnetPropertyIds.PROP_PRESENT_VALUE, out Value);

                                if (ret)
                                {
                                    ResData.Add(ObjType.ToString() + ":" + oi[1], Value.ToString());
                                }
                            }
                            // mqtt publish
                            string strValue = Convert.ToString(JsonConvert.SerializeObject(ResData));
                            MqttClient.Publish("Bacnet/Raspberry/" + voDevice.ToString() + "/Datas", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                        }
                    }
                }
                Thread.Sleep(interv);
            }
        }

        static void RestartApp(int pid, string applicationName)
        {
            // Wait for the process to terminate
            Process process = null;
            try
            {
                process = Process.GetProcessById(pid);
                process.WaitForExit(1000);
            }
            catch (ArgumentException ex)
            {
                // ArgumentException to indicate that the 
                // process doesn't exist?   LAME!!
            }
            Process.Start(applicationName, "");
        }

        private static async void OnIAm(BacnetClient sender, BacnetAddress adr,
            uint deviceid, uint maxapdu, BacnetSegmentations segmentation, ushort vendorid)
        {
            //Console.WriteLine($"Detected device {deviceid} at {adr}");
            try
            {
                lock (DevicesList)
                {
                    // Device already registred ?
                    foreach (BacNode bn in DevicesList)
                        if (bn.getAdd(deviceid) != null) return;   // Yes

                    // Not already in the list
                    DevicesList.Add(new BacNode(adr, deviceid));   // add it
                }

                // In theory each bacnet device should have object of type OBJECT_DEVICE with property PROP_OBJECT_LIST
                // This property is a list of all bacnet objects (ids) of that device
                //var deviceObjId = new BacnetObjectId(BacnetObjectTypes.OBJECT_DEVICE, deviceid);
                //var objectIdList = await client.ReadPropertyAsync(adr, deviceObjId, BacnetPropertyIds.PROP_OBJECT_LIST);

                //var objectIdList = new LinkedList<BacnetObjectId>();
                //client.ReadPropertyRequest(adr, deviceObjId, BacnetPropertyIds.PROP_OBJECT_LIST, out value_list, arrayIndex: 0);
                //var objectCount = value_list.First().As<uint>();

                //for (uint a = 1; a <= objectCount; a++)
                //{
                //    client.ReadPropertyRequest(adr, deviceObjId, BacnetPropertyIds.PROP_OBJECT_LIST, out value_list, arrayIndex: a);
                //    objectIdList.AddLast(value_list.First().As<BacnetObjectId>());
                //}

                //Dictionary<int, Dictionary<string, string>> ResData = new Dictionary<int, Dictionary<string, string>>();
                //Dictionary<string, string> ResDataObjName = new Dictionary<string, string>();
                //int i = 0;
                //foreach (var objId in objectIdList)
                //{
                //    Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                //    //Console.WriteLine($"{objId}");
                //    string OBJ = objId.ToString();

                //    string[] oi = OBJ.Split(':');
                //    uint instance = Convert.ToUInt32(oi[1]);
                //    BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), oi[0]);
                //    var voObjId = new BacnetObjectId(ObjType, instance);
                //    var objectName = await sender.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_NAME);
                //    ResDataObj.Add("ObjType", OBJ);
                //    ResDataObj.Add("ObjName", objectName[0].ToString());
                //    ResData.Add(i, ResDataObj);
                //    i++;
                //}
            }
            catch (Exception e)
            {
                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("deviceID", deviceid.ToString());
                voResData.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                MqttClient.Publish("Bacnet/Raspberry/" + deviceid + "/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
        }
        private static void setConfig(string message)
        {
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());
            string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";

            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("MQTTHOST", voobj["MQTTHOST"].ToString());
            voResData.Add("PatchFile", voobj["PatchFile"].ToString());
            voResData.Add("Interval", voobj["Interval"].ToString());

            // begin write to json file
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            File.WriteAllText(basepatch, strValue);
            // end wriet to json file

            // publish mqtt
            MqttClient.Publish("Bacnet/Raspberry/All/setConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            // Restart Apps
            Process currentProcess = Process.GetCurrentProcess();
            int pid = currentProcess.Id;
            string applicationName = currentProcess.ProcessName;
            RestartApp(pid, applicationName);
            System.Environment.Exit(1);
        }
        private static void getConfig(string message)
        {
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());

            string basepatch = AppDomain.CurrentDomain.BaseDirectory + "AppSetting.json";
            if (File.Exists(basepatch))
            {
                string Json = File.ReadAllText(basepatch);
                if (Json != "null")
                {
                    object rootresult = JsonConvert.DeserializeObject(Json);
                    JObject rootobj = JObject.Parse(rootresult.ToString());
                    string poMQTT = rootobj["MQTTHOST"].ToString();
                    string poPatch = rootobj["PatchFile"].ToString();
                    string poIntv = rootobj["Interval"].ToString();
                    // publish mqtt
                    Dictionary<string, string> voResData = new Dictionary<string, string>();
                    voResData.Add("MQTTHOST", poMQTT);
                    voResData.Add("PatchFile", poPatch);
                    voResData.Add("Interval", poIntv);
                    string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                    MqttClient.Publish("Bacnet/Raspberry/All/getConfigData", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
        }
        private static async void whoisDevice(string message)
        {
            Dictionary<int, Dictionary<string, string>> ResData = new Dictionary<int, Dictionary<string, string>>();
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());
            uint deviceid = Convert.ToUInt32(voobj["deviceID"].ToString());
            BacnetAddress adr;

            try
            {
                // Looking for the device
                adr = DeviceAddr((uint)deviceid);
                if (adr == null) { throw new Exception("Device not found or disconect from network"); }

                var deviceObjId = new BacnetObjectId(BacnetObjectTypes.OBJECT_DEVICE, deviceid);
                //var objectIdList = await client.ReadPropertyAsync(adr, deviceObjId, BacnetPropertyIds.PROP_OBJECT_LIST);

                var objectIdList = new LinkedList<BacnetObjectId>();
                client.ReadPropertyRequest(adr, deviceObjId, BacnetPropertyIds.PROP_OBJECT_LIST, out value_list, arrayIndex: 0);
                var objectCount = value_list.First().As<uint>();

                for (uint a = 1; a <= objectCount; a++)
                {
                    client.ReadPropertyRequest(adr, deviceObjId, BacnetPropertyIds.PROP_OBJECT_LIST, out value_list, arrayIndex: a);
                    objectIdList.AddLast(value_list.First().As<BacnetObjectId>());
                }

                Dictionary<string, string> ResDataObjName = new Dictionary<string, string>();
                int i = 0;
                foreach (var objId in objectIdList)
                {
                    Dictionary<string, string> ResDataObj = new Dictionary<string, string>();
                    //Console.WriteLine($"{objId}");
                    string OBJ = objId.ToString();

                    string[] oi = OBJ.Split(':');
                    uint instance = Convert.ToUInt32(oi[1]);
                    BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), oi[0]);
                    var voObjId = new BacnetObjectId(ObjType, instance);
                    var objectName = await client.ReadPropertyAsync(adr, voObjId, BacnetPropertyIds.PROP_OBJECT_NAME);
                    ResDataObj.Add("ObjType", OBJ);
                    ResDataObj.Add("ObjName", objectName[0].ToString());
                    ResData.Add(i, ResDataObj);
                    i++;
                }
                // publish to mqtt
                string strValue = Convert.ToString(JsonConvert.SerializeObject(ResData));
                MqttClient.Publish("Bacnet/Raspberry/" + deviceid + "/WhoisDatas", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            catch (Exception e)
            {
                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("deviceID", voobj["deviceID"].ToString());
                voResData.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                MqttClient.Publish("Bacnet/Raspberry/" + deviceid + "/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }

        }
        static void ReadToDevice(string message)
        {
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());
            try
            {
                BacnetValue Value;
                bool ret;
                int DeviceID = Convert.ToInt32(voobj["deviceID"].ToString());
                string[] poObj = voobj["ObjectType"].ToString().Split(':');
                uint instance = Convert.ToUInt32(poObj[1]);
                BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), poObj[0]);
                ret = ReadScalarValue(DeviceID, new BacnetObjectId(ObjType, instance), BacnetPropertyIds.PROP_PRESENT_VALUE, out Value);
                if (ret == true)
                {
                    //Console.WriteLine("Read (" + DeviceID + ") value : " + Value.Value.ToString());
                    // publish to mqtt
                    string strValue = "{'deviceID':'" + DeviceID + "','ObjectType':'" + voobj["ObjectType"].ToString() + "','value':'" + Value.Value.ToString() + "'}";
                    MqttClient.Publish("Bacnet/Raspberry/" + DeviceID + "/ReadDatas", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
            catch (Exception e)
            {
                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("deviceID", voobj["deviceID"].ToString());
                voResData.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                MqttClient.Publish("Bacnet/Raspberry/" + voobj["deviceID"].ToString() + "/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
        }

        static void WriteToDevice(string message)
        {
            object result = JsonConvert.DeserializeObject(message);
            JObject voobj = JObject.Parse(result.ToString());
            try
            {
                BacnetValue Value;
                bool ret;
                int DeviceID = Convert.ToInt32(voobj["deviceID"].ToString());
                string[] poObj = voobj["ObjectType"].ToString().Split(':');
                uint instance = Convert.ToUInt32(poObj[1]);
                BacnetObjectTypes ObjType = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), poObj[0]);
                int voValue = Convert.ToInt32(voobj["value"].ToString());
                BacnetValue newValue = new BacnetValue(Convert.ToSingle(voValue));
                ret = WriteScalarValue(DeviceID, new BacnetObjectId(ObjType, instance), BacnetPropertyIds.PROP_PRESENT_VALUE, newValue);
                if (ret == true)
                {
                    //Console.WriteLine("Write feedback : " + ret.ToString());
                    // publish to mqtt
                    string strValue = "{'deviceID':'" + DeviceID + "','ObjectType':'" + voobj["ObjectType"].ToString() + "','value':'" + ret.ToString() + "'}";
                    MqttClient.Publish("Bacnet/Raspberry/" + DeviceID + "/WriteDatas", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                }
            }
            catch (Exception e)
            {
                Dictionary<string, string> voResData = new Dictionary<string, string>();
                voResData.Add("deviceID", voobj["deviceID"].ToString());
                voResData.Add("msg", e.Message);
                string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
                MqttClient.Publish("Bacnet/Raspberry/" + voobj["deviceID"].ToString() + "/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
        }
        static bool ReadScalarValue(int device_id, BacnetObjectId BacnetObjet, BacnetPropertyIds Propriete, out BacnetValue Value)
        {
            BacnetAddress adr;
            IList<BacnetValue> NoScalarValue;

            Value = new BacnetValue(null);

            // Looking for the device
            adr = DeviceAddr((uint)device_id);
            if (adr == null) return false;  // not found

            // Property Read
            if (client.ReadPropertyRequest(adr, BacnetObjet, Propriete, out NoScalarValue) == false)
                return false;

            Value = NoScalarValue[0];
            return true;
        }

        static bool WriteScalarValue(int device_id, BacnetObjectId BacnetObjet, BacnetPropertyIds Propriete, BacnetValue Value)
        {
            BacnetAddress adr;

            // Looking for the device
            adr = DeviceAddr((uint)device_id);
            if (adr == null) return false;  // not found

            // Property Write
            BacnetValue[] NoScalarValue = { Value };
            if (client.WritePropertyRequest(adr, BacnetObjet, Propriete, NoScalarValue) == false)
                return false;

            return true;
        }

        static BacnetAddress DeviceAddr(uint device_id)
        {
            BacnetAddress ret;

            lock (DevicesList)
            {
                foreach (BacNode bn in DevicesList)
                {
                    ret = bn.getAdd(device_id);
                    if (ret != null) return ret;
                }
                // not in the list
                return null;
            }
        }

        public static bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        protected override void OnStop()
        {
            Dictionary<string, string> voResData = new Dictionary<string, string>();
            voResData.Add("deviceID", "Bacnet");
            voResData.Add("msg", "HASIOT - Bacnet is stopped");
            string strValue = Convert.ToString(JsonConvert.SerializeObject(voResData));
            MqttClient.Publish("Bacnet/Raspberry/Bacnet/Error", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }

        class BacNode
        {
            BacnetAddress adr;
            public uint device_id { get; set; }

            public BacNode(BacnetAddress adr, uint device_id)
            {
                this.adr = adr;
                this.device_id = device_id;
            }

            public BacnetAddress getAdd(uint device_id)
            {
                if (this.device_id == device_id)
                    return adr;
                else
                    return null;
            }
        }
    }
}
